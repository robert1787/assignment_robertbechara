##Snake Attack

**Game Description**

The game is a simple snake game, where the player needs to eat random spawned food on a specific grid.
There are multiple types of food;example red, blue, etc... And each specific food gives a specifc amount of score,
if the player eats the same type of food the he will get a bounce score.

---

**Organization**

I try to organize my game based on elements. Meaning, each state of the game in its own folder.
Main menu folder where it contains all the scripts and scene and assets relative only to mainmenu.
And An GamePlay folder where it holds all the game play elements like the food, snake, Camera,etc..
In Each element has its own scripts, Models, Texture, prefabs and animation...

---

**Main Menu**

In the main menu scene, we have these scripts:

1. GameManager
2. GameData
3. UserData
4. UIMainMenu
5. UIHighScore

*GameManager* is a Singelton, where there are 2 required components "GameData" and "UserData", since the game cannot run without those scripts.
The GameManager is seponsable for loading the new data from the streaming data and sending is to the GameData.
Setting the new score and getting it, from the "UserData". And Loading the Level Scene.
Note: This is DontDestroyOnLoad to reuse it in the other scenes

*UserData* this is were all the user data is saved, coins, gems, highscore, unlocked levels, etc..
But since this game only has score as a player data there are 2 functions, one that sets the highscore and save it to 
PlayerPrefs , and the other that retrieves it.

*GameData* here is where you should referance all the food or anything that is based on data for the game.
It also updates the data of the food based on a JSON file. 
How it updates:
The JSON is formed out of a list of FoodInfo { FoodType, score, color }
It loops throw the List of food and check its foodtype if they are the same and updates the value accordingly.
Note: An editor script was created to help me generate the JSON File based on the referenced Food. So this Json 
can be changed and then send to the server and the player would avoid updating the game just for value changes.

*UIMainMenu* this mainly handles the UI of the main menu, since the GameManager hopes from one scene to another,
referencing the button from the scene is a no go, therefore the UIMainMenu handles it by dynamically adding an
event from the GameManager. This checks if there is a highscore if not center the play button else send it to the
UIHighScore script.

*UIHighScore* This handles displaying or removing the highscore panel and updating the text of the highscore.

---

**In Game**

Gameplay has only one scene called "Level".
In the Level scene, we have these scripts:

1. GamePlayManager
2. GridManager/GroundCell
3. UIInGame
4. TouchController
5. PlayerController/BodyPiece
6. CameraController
7. Food

*GamePlayManager* is a Singelton where it has a referance to the grid and the In game UI.It also has the function to go back
to main menu.

*GridManager* is responsable for converting world position to grid position.On Start creates the grid from walls to cells. 
It holds all the grid cells and it triggers the animation of the grid. It sets the neighbors cells for each groundcell.
The grid can create a food that is randomly chosen from the GameData and not colliding with the snake.
*GroundCell* create a dynamical material and has a list of neighbor cells which are populated from the GridManager.
A simple animation is done when the color is changed and the all neighboring cell would get trigger to continue the animation.
Note: The wall has a Tag Of "Obstacle"

*UIInGame* handles the score display and the streak. Opens also the finish panel.

*TouchController* this handles all the touch inputs of the player. It holds the initial touch point, then on draging the mouse/fingue 
calculate the deltaPosition so see which direction the user swiped.
I am using a touchstate to keep track for where the use is at.
On release the finguer or mouse I am calculating the Dot product between predefined axis in a list and checking which one is the nearest
to the swip and then calling an Event where the player move would hook to it.
Nore:I am converting all the touch point to the world location to avoid working with pixels since every device has its own number of pixels
and thuse resulting in a uniformed experience for the user.

*BodyPiece* Is a simple script where is would apply mimic the scale and position of a scepific BodyPiece. This is place on each body piece 
of the sanke.
Note: The Body prefab has a Tag of "Obstacle"
*PlayerController* This inherit from the BodyPiece since it is a piece of the snake mainly the head. Yet this piece has more functionalities
then the other piece, it moves the head based on a direction, and this direction is hook to the TouchController. The moving system is in the 
FixedUpdate() since we have a fixed amount between each step, the game speed is predefined before in the project setting.
The player has coordinates and these coordinates are sent to the girdmanager to get the correct location.
There is a list of BodyPiece which corresponds to the length of the snakes. Then after moving the head, we loop through each BodyPiece in descending
order and send its front piece to itself to move forward.
The Head has a rigidbody2D thus we can use the OnCollisionEnter2D and in this function when the head collides with an obstacle Taged object 
The script would be disabled then update the highscore then open the finish panel. Yet if the tag is "Food" we add a piece by creating a new
piece and adding it to the BodyPiece List. Then we create a new food from the grid.
On Collecting the food I get the score then I check if the type is the same like the previous if not set the streak to zero, set the currentType, and add the score
else add the streak and multiply it by the score, and then we destroy the Food.
OnChangeScore is Invoke so that the UI would be updated. And plus the camera is hooked on the same event

*CameraController* Camera is set in the middle on start and has a shake animation triggered when a food is collected

*Food* Food has a base prefab where from it we create variant prefabs Blue, Red foods... It Has a Tag "Food" and a class that holds the food data

---

**Way Of Working**

First I create a prototype of the gameplay.
	1. Controller like touch
	2. Grid creation
	3. How the player moves on the grid
	4. Death/Reset player
	5. ...

Then I start adding the main scene and the data base

---