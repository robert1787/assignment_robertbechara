using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchController : MonoBehaviour
{
    enum ControllerState { Idle, Swipping, Cancel}

    [SerializeField] float _minDistanceToRegistorSwip = 3;
    [SerializeField] Camera _mainCamera;
    public UnityEvent<Vector3> OnChangedDirection = new UnityEvent<Vector3>();

    float _touchTimeElapsed = 0;
    Vector3 _startTouchPosition = new Vector3();
    Vector3 _deltaPosition;
    ControllerState _controllerState;

    private readonly Vector3[] _axis = new Vector3[4] { Vector3.up, Vector3.down, Vector3.right, Vector3.left };
    
    public Vector3 ConvertTouchLocationToGameLocation(Vector3 position)
    {
        var ray = _mainCamera.ScreenPointToRay(position);
        Plane plane = new Plane(Vector3.forward, Vector3.zero);
        if(plane.Raycast(ray, out float enter)){
            return ray.origin + ray.direction * enter;
        }
        return ray.origin + ray.direction * 10;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_startTouchPosition, 1);
        Gizmos.DrawWireSphere(_startTouchPosition + _deltaPosition, 1);
    }

    public void FinishedSwipe()
    {
        //Was not in swipping mode
        if (_controllerState != ControllerState.Swipping)
            return;

        //Distance was so small that the swip need to be cancelled
        if(_deltaPosition.sqrMagnitude < _minDistanceToRegistorSwip * _minDistanceToRegistorSwip)
        {
            _controllerState = ControllerState.Cancel;
            return;
        }

        _controllerState = ControllerState.Idle;
        _deltaPosition.Normalize();
        float largest_dot = 0;
        int selectAxis = -1;
        for(int i = 0; i < _axis.Length; i++)
        {
            float dot = Vector3.Dot(_deltaPosition, _axis[i]);
            if(i == -1)
            {
                largest_dot = dot;
            }else if(largest_dot < dot)
            {
                largest_dot = dot;
                selectAxis = i;
            }
        }
        //Send the direction 
        OnChangedDirection.Invoke(_axis[selectAxis]);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            _controllerState = ControllerState.Swipping;
            _startTouchPosition = ConvertTouchLocationToGameLocation(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            if (_controllerState == ControllerState.Swipping)
            {
                //Add deltaTime to check how much time has passed by
                _touchTimeElapsed += Time.deltaTime;
                _deltaPosition = ConvertTouchLocationToGameLocation(Input.mousePosition) - _startTouchPosition;

                if (_touchTimeElapsed > 1)
                {
                    FinishedSwipe();
                }
            }
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            FinishedSwipe();
            //Reset the elapsed time
            _touchTimeElapsed = 0;
        }
#else
        if(Input.touchCount > 0){
            var touch = Input.GetTouch(0);
            switch(touch.phase){
                case TouchPhase.Began:
                    _controllerState = ControllerState.Swipping;
                    _startTouchPosition = ConvertTouchLocationToGameLocation(touch.position);
                    break;
                case TouchPhase.Stationary:
                case TouchPhase.Moved:
                    if (_controllerState == ControllerState.Swipping)
                    {
                        //Add deltaTime to check how much time has passed by
                        _touchTimeElapsed += Time.deltaTime;
                        _deltaPosition = ConvertTouchLocationToGameLocation(touch.position) - _startTouchPosition;

                        if (_touchTimeElapsed > 1)
                        {
                            FinishedSwipe();
                        }
                    }
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    FinishedSwipe();
                    //Reset the elapsed time
                    _touchTimeElapsed = 0;
                    break;
            }
        }
#endif
    }
}
