using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : BodyPiece
{
    //Initial States
    [SerializeField] int _intialSize = 4;
    [SerializeField] BodyPiece _bodyPiecePrefab;

    //Public Events
    public UnityEvent<int, int, Color> OnChangedScore = new UnityEvent<int, int, Color>();
    public UnityEvent<Vector2Int, Color> OnCollectedFood = new UnityEvent<Vector2Int, Color>();

    GridManager _gridManager;
    GameManager _gameManager;

    //Head data
    Vector2Int _direction = new Vector2Int(1,0);
    List<BodyPiece> _snakeParts = new List<BodyPiece>();

    //Player data
    int _score;
    FoodType _streakType = FoodType.NONE;
    int _streak = 0;

    // Start is called before the first frame update
    public void SetUpPlayer()
    {
        _gridManager = GamePlayManager.instance.gridManager;
        _gameManager = GameManager.instance;

        ResetSnake();
    }
    
    void FixedUpdate()
    {
        for (int i = _snakeParts.Count - 1; i > 0; i--)
        {
            _snakeParts[i].UpdateBodyPiece(_snakeParts[i - 1]);
        }
        _currentCoordinates += _direction;
        if(extraScale > 1)
        {
            extraScale -= 0.2f;
            transform.localScale = Vector3.one * extraScale;
        }
        UpdatePositionBasedOnCoordinates();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            //Death
            enabled = false;
            GamePlayManager.instance.OpenFinishPanel(_gameManager.UpdateScore(_score), _gameManager.GetHighScore());
            return;
        }
        if (collision.gameObject.CompareTag("Food"))
        {
            //Add piece and add score
            var food = collision.gameObject.GetComponent<Food>();
            if (food)
                AddPiece(food);
            else
                Debug.LogError("No Food Component Was found", collision.gameObject);
            //Added a new food
            _gridManager.CreateFood();
        }
    }

    //Update the direction
    public void SetDirection(Vector3 newDirection)
    {
        //To force only going right or left
        if (Vector3.Dot(newDirection, new Vector3(_direction.x, _direction.y)) != 0)
            return;

        _direction = new Vector2Int((int)newDirection.x, (int)newDirection.y);
    }

    private void UpdatePositionBasedOnCoordinates()
    {
        var newPosition = _gridManager.GetGridLocation(_currentCoordinates);
        newPosition.z = transform.position.z;
        transform.position = newPosition;
    }

    private void ResetSnake()
    {
        //Reset scoring
        _streak = 0;
        _streakType = FoodType.NONE;
        _score = 0;

        for (int i = 1; i < _snakeParts.Count; i++)
        {
            Destroy(_snakeParts[i].gameObject);
        }
        _snakeParts = new List<BodyPiece>();
        //Set the players location at the center
        _currentCoordinates = _gridManager.GetCenterCoordinates();
        UpdatePositionBasedOnCoordinates();

        //Add the head to the list
        _snakeParts.Add(this);

        //Create the extra parts
        for (int i = 1; i < _intialSize; i++)
        {
            AddPiece(null);
        }
    }

    public bool CheckOverLapping(Vector2Int randomCoordinates)
    {
        foreach(var part in _snakeParts)
        {
            if(part.GetCoordinates() == randomCoordinates)
            {
                return true;
            }
        };
        return false;
    }

    private void AddPiece(Food collected)
    {
        var curBody = Instantiate(_bodyPiecePrefab, transform.position, Quaternion.identity);
        _snakeParts.Add(curBody);
        if (collected)
        {
            if(_streakType != collected.foodInfo.foodType)
            {
                _streak = 0;
                _streakType = collected.foodInfo.foodType;
            }
            _streak++;
            _score += collected.foodInfo.score * _streak;
            OnChangedScore.Invoke(_score, _streak, collected.foodInfo.color);
            OnCollectedFood.Invoke(_currentCoordinates, collected.foodInfo.color);
            Destroy(collected.gameObject);
            extraScale = 2;
            transform.localScale = Vector3.one * extraScale;
        }
    }

}
