using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPiece : MonoBehaviour
{
    Transform _transform;
    protected Vector2Int _currentCoordinates = new Vector2Int();
    [HideInInspector]
    public float extraScale = 1;

    void Awake()
    {
        _transform = transform;
    }

    public void UpdateBodyPiece(BodyPiece piece)
    {
        _transform.position = piece.GetPosition(out _currentCoordinates);
        extraScale = piece.extraScale;
        _transform.localScale = Vector3.one * extraScale;
    }

    public Vector3 GetPosition(out Vector2Int coordinates)
    {
        coordinates = _currentCoordinates;
        return _transform.position;
    }

    public  Vector2Int GetCoordinates()
    {
        return _currentCoordinates;
    }
}
