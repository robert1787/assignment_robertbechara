using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCell : MonoBehaviour
{
    public List<GroundCell> neighborCells = new List<GroundCell>();
    public int ID;

    [SerializeField] MeshRenderer _meshRenderer;

    int _idSetter = -1;
    Color _newColor;
    Material _currentMat;
    Transform _transform;
    Coroutine _moveCoroutine;

    private void Awake()
    {
        _transform = transform;
        _currentMat = new Material(_meshRenderer.material);
        _meshRenderer.material = _currentMat;
    }

    public void AddNeighborCell(GroundCell toAdd)
    {
        if (neighborCells.Contains(toAdd))
            return;

        neighborCells.Add(toAdd);
        toAdd.AddNeighborCell(this);
    }

    public void SetColor(Color color, int id)
    {       
        //Was able to set self check my neighbors Next Fram
        if(CanUpdateColor(color, id))
        {
            if(_moveCoroutine != null)
            {
                StopCoroutine(_moveCoroutine);
            }

            _moveCoroutine = StartCoroutine(CheckNeighbors());
        }
    }

    IEnumerator CheckNeighbors()
    {
        float t = 0;
        float maxTime = 0.2f;
        float offSet = .5f;
        Vector3 initial = transform.localPosition;

        while (t < maxTime)
        {
            t += Time.deltaTime;
            Vector3 p = initial;
            p.z = t / maxTime * offSet - offSet;
            transform.localPosition = p;
            yield return null;
        }
        transform.localPosition = initial;

        //Set For the neighbors
        foreach (var n in neighborCells)
        {
            n.SetColor(Color.Lerp(_newColor, Color.white, 0.05f), _idSetter);
        }
    }

    private bool CanUpdateColor(Color color, int id)
    {
        if(id == _idSetter)
        {
            return false;
        }

        _idSetter = id;
        _newColor = color;
        _currentMat.color = _newColor;
        return true;
    }

    public void SetLocalScale(Vector3 localScale)
    {
        _transform.localScale = localScale;
    }

    public void SetParent(Transform parent)
    {
        _transform.SetParent(parent);
    }

    private void Update()
    {
        _currentMat.color = Color.Lerp(_currentMat.color, Color.white, Time.deltaTime);
    }
}
