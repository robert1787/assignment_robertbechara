using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    //Scene Refs
    [SerializeField] PlayerController _player;

    [SerializeField] Transform _wallPrefab;
    [SerializeField] GroundCell _groundPrefab;
    [SerializeField] Vector2 _cellSize = new Vector2(2, 2);
    [SerializeField] Vector2Int _gridSize = new Vector2Int(20, 20);

    GroundCell[] groundGrid;
    GameManager _gameManager;
    
    void Start()
    {
        _gameManager = GameManager.instance;
        groundGrid = new GroundCell[_gridSize.x * _gridSize.y];
        //Set Camera and the grip visuals to the center of the grid
        transform.position = GetCenterLocation();

        //Create Ground
        for (int y = 0; y < _gridSize.y; y++)
        {
            for (int x = 0; x < _gridSize.x; x++)
            {
                var ground = Instantiate(_groundPrefab, GetGridLocation(x, y), Quaternion.identity);
                ground.SetLocalScale(new Vector3(_cellSize.x, _cellSize.y, 1));
                ground.SetParent(transform);
                ground.ID = x + y * _gridSize.x;
                groundGrid[ground.ID] = ground;
                if (x != 0)
                {
                    int Nx = x - 1;
                    ground.AddNeighborCell(groundGrid[Nx + y * _gridSize.x]);
                }
                if (y != 0)
                {
                    int Ny = y - 1;
                    ground.AddNeighborCell(groundGrid[x + Ny * _gridSize.x]);
                }
            }
        }

        //Set Up the walls
        //Build the right Wall
        CreateWall(new Vector2Int(_gridSize.x, _gridSize.y / 2), new Vector3(_cellSize.x, (_gridSize.y + 2) * _cellSize.y, _cellSize.x));
        //Build The left wall
        CreateWall(new Vector2Int(-1, _gridSize.y / 2), new Vector3(_cellSize.x, (_gridSize.y + 2) * _cellSize.y, _cellSize.x));
        //Build The Upper wall
        CreateWall(new Vector2Int(_gridSize.x / 2, _gridSize.y), new Vector3((_gridSize.x + 2) * _cellSize.x, _cellSize.y, _cellSize.x));
        //Build The lower wall
        CreateWall(new Vector2Int(_gridSize.x / 2, -1), new Vector3((_gridSize.x + 2) * _cellSize.x, _cellSize.y, _cellSize.x));

        //Initialize Player
        _player.SetUpPlayer();

        //Create the First Food
        CreateFood();
    }

    public void SetColorToCell(Vector2Int cellCoor, Color cellColor)
    {
        var selectedID = cellCoor.x + cellCoor.y * _gridSize.x;
        groundGrid[selectedID].SetColor(cellColor, selectedID);
    }

    private void CreateWall(Vector2Int GridLocation, Vector3 localScale)
    {
        var sidePosition = GetGridLocation(GridLocation);
        var wall = Instantiate(_wallPrefab, sidePosition, Quaternion.identity);
        wall.localScale = localScale;
        wall.SetParent(transform);
    }

    public void CreateFood()
    {
        Food toCreate = _gameManager.GetRandomFoodType();
        Vector2Int randomCoordinates = GetRandomCoordinates();
        int b = 0;
        //Check if on player
        while (_player.CheckOverLapping(randomCoordinates) && b < 20)
        {
            randomCoordinates = GetRandomCoordinates();
            b++;
        }
        var newP = GetGridLocation(randomCoordinates);
        newP.z = _player.transform.position.z;
        Instantiate(toCreate, newP, Quaternion.identity);
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < _gridSize.x; i++)
        {
            for (int j = 0; j < _gridSize.y; j++)
                Gizmos.DrawWireCube(GetGridLocation(i, j), _cellSize);
        }
    }

    public Vector2Int GetRandomCoordinates()
    {
        return new Vector2Int(Random.Range(0, _gridSize.x), Random.Range(0, _gridSize.y));
    }

    public Vector2Int GetCenterCoordinates()
    {
        return new Vector2Int(_gridSize.x / 2, _gridSize.y / 2);
    }

    public Vector3 GetCenterLocation()
    {
        return GetGridLocation(GetCenterCoordinates());
    }

    public Vector3 GetGridLocation(Vector2Int coordinates)
    {
        return GetGridLocation(coordinates.x, coordinates.y);
    }

    public Vector3 GetGridLocation(int x, int y)
    {
        return new Vector3(x * _cellSize.x, y * _cellSize.y, 0) + (Vector3)_cellSize/2;
    }

}
