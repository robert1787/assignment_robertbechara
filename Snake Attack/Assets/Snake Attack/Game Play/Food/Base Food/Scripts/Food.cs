using UnityEngine;

public class Food : MonoBehaviour
{
    public FoodInfo foodInfo;

    [SerializeField] Material _foodMat;
    [SerializeField] MeshRenderer _foodMeshRenderer;

    private void Start()
    {
        var mat = new Material(_foodMat);
        mat.color = foodInfo.color;
        _foodMeshRenderer.material = mat;
    }
}