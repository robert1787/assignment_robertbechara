using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInGame : MonoBehaviour
{
    GameManager _gameManager;

    [SerializeField] List<TextMeshProUGUI> _scoreDisplayText;
    [SerializeField] TextMeshProUGUI _streakDisplayText, _highScoreDisplay;
    [SerializeField] Image _streakImage;
    [SerializeField] GameObject _finishPanel, _finishHighscoreParticles;

    // Start is called before the first frame update
    void Awake()
    {
        _gameManager = GameManager.instance;
    }

    public void OpenFinishPanel(bool gotHighscore, int highScore)
    {
        _finishPanel.SetActive(true);
        _finishHighscoreParticles.SetActive(gotHighscore);
        _highScoreDisplay.text = highScore.ToString();
    }
    
    public void UpdateScore(int score, int streak, Color food)
    {
        _streakImage.gameObject.SetActive(true);
        _streakImage.color = food;
        _scoreDisplayText.ForEach(current => current.text = score.ToString());
        _streakDisplayText.text = streak.ToString();
    }
}
