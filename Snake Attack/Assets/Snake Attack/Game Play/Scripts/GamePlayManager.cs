using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayManager : MonoBehaviour
{
    public static GamePlayManager instance { get; set; }

    public GridManager gridManager;
    [SerializeField] UIInGame _inGameUI;

    void Awake()
    {
        if (instance)
        {
            if (instance != this)
            {
                Debug.LogWarning("There was two Grid Manager. Need to destroy the second one.", gameObject);
            }
        }
        else
        {
            instance = this;
        }
    }

    public void OpenFinishPanel(bool newHighScore, int highScore)
    {
        _inGameUI.OpenFinishPanel(newHighScore, highScore);
    }

    public void PressedOKButton()
    {
        SceneManager.LoadScene("Main Menu");
    }

}
