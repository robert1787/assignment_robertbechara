using UnityEngine;

public class CameraController : MonoBehaviour
{
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        Vector3 cameraCenter = GamePlayManager.instance.gridManager.GetCenterLocation();
        cameraCenter.z = transform.position.z;
        transform.position = cameraCenter;
    }
    
    public void ShakeCamera()
    {
        animator.SetTrigger("Shake");
    }

}
