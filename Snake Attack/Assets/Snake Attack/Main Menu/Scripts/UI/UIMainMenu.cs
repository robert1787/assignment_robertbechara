using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
    [SerializeField] Button _startButton;
    [SerializeField] UIHighScore _highScorePanel;

    GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameManager.instance;
        _startButton.onClick.AddListener(_gameManager.StartGame);

        var highScore = _gameManager.GetHighScore();

        //Center the start button if there is not high score
        if(highScore < 0)
        {
            var bRect = _startButton.GetComponent<RectTransform>();
            var rectLocation = bRect.localPosition;
            rectLocation.x = 0;
            bRect.localPosition = rectLocation;
        }

        //Update the score
        _highScorePanel.SetScore(highScore);
    }
    
}
