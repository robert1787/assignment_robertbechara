using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIHighScore : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _highScoreDiplay;
    
    public void SetScore(int score)
    {
        gameObject.SetActive(score > 0);
        _highScoreDiplay.text = score.ToString();
    }
    
}
