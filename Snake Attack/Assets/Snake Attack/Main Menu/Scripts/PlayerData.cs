using UnityEngine;

public class PlayerData : MonoBehaviour
{
    /// <summary>
    /// Update the score of the player 
    /// </summary>
    /// <param name="score">The current match score</param>
    /// <returns> If got a high score</returns>
    public bool UpdateScore(int score)
    {
        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        if(score > highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
            return true;
        }
        return false;
    }

    public int GetHighScore()
    {
        return PlayerPrefs.GetInt("HighScore", -1);
    }

}
