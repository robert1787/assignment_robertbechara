using System;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

public enum FoodType { Red, Blue, NONE }

[Serializable]
public class FoodInfo
{
    public FoodType foodType;
    public int score;
    public Color color;
}

public class GameData : MonoBehaviour
{
    [Serializable]
    public class FoodData
    {
        public List<FoodInfo> foods;

        public FoodData()
        {
            foods = new List<FoodInfo>();
        }

        public FoodData(List<Food> allFoodTypes)
        {
            foods = new List<FoodInfo>();
            allFoodTypes.ForEach(food => foods.Add(new FoodInfo
            {
                foodType = food.foodInfo.foodType,
                score = food.foodInfo.score,
                color = food.foodInfo.color
            }));
        }
    }

    [SerializeField] List<Food> _allFoodTypes = new List<Food>();

    public void UpdateAllFoodData(string foodDataString)
    {
        try
        {
            var foodData = JsonUtility.FromJson<FoodData>(foodDataString);
            foodData.foods.ForEach(serverFood =>
            {
                foreach(var localFood in _allFoodTypes)
                {
                    if(localFood.foodInfo.foodType == serverFood.foodType)
                    {
                        //Update the Food info
                        localFood.foodInfo.score = serverFood.score;
                        localFood.foodInfo.color = serverFood.color;
                        break;
                    }
                };
            });
        }
        catch (Exception e)
        {
            Debug.LogError(e.StackTrace);
        }
    }

    public string GenerateJSONFileData()
    {
        FoodData data = new FoodData(_allFoodTypes);
        return JsonUtility.ToJson(data);
    }

    public Food GetRandomFood()
    {
        if (_allFoodTypes.Count == 0)
            Debug.LogError("No Food Are referenced in the Game data. Need to reference sone food");

        return _allFoodTypes[Random.Range(0, _allFoodTypes.Count)];
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameData))]
public class GameDataEditor : Editor
{
    GameData _gameData;

    private void OnEnable()
    {
        _gameData = target as GameData;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Generate JSON"))
        {
            string data = _gameData.GenerateJSONFileData();
            if (!Directory.Exists(Application.streamingAssetsPath))
            {
                //if it doesn't, create it
                Directory.CreateDirectory(Application.streamingAssetsPath);

            }
            File.WriteAllText(Application.streamingAssetsPath + "/GameData.json", data);
            AssetDatabase.Refresh();
        }
    }
}
#endif