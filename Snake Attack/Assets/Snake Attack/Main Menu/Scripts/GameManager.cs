using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameData))]
[RequireComponent(typeof(PlayerData))]
public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; set; }

    GameData _gameData;
    PlayerData _playerData;

    void Awake()
    {
        if (instance)
        {
            if (instance != this)
            {
                Debug.LogWarning("There was two Grid Manager. Need to destroy the second one.", gameObject);
            }
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
        _gameData = GetComponent<GameData>();
        _playerData = GetComponent<PlayerData>();
    }

    private IEnumerator Start()
    {
        //Load the from streaming assets
        string url = Path.Combine(Application.streamingAssetsPath, "GameData.json");
        string data = "";
        
        //Check if we should use UnityWebRequest or File.ReadAllBytes
        if (url.Contains("://") || url.Contains(":///"))
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();
            data = www.downloadHandler.text;
        }
        else
        {
            data = File.ReadAllText(url);
        }
        _gameData.UpdateAllFoodData(data);
    }

    #region Main Menu
    public void StartGame()
    {
        SceneManager.LoadScene("Level");
    }

    public int GetHighScore()
    {
        return _playerData.GetHighScore();
    }
    #endregion

    #region In Game

    public Food GetRandomFoodType()
    {
        return _gameData.GetRandomFood();
    }

    public bool UpdateScore(int score)
    {
        return _playerData.UpdateScore(score);
    }

    #endregion

}